# Experimental Trading Notebooks

This repository contains experiments on some trading and market modelling ideas.

## Install

Initialize a new `venv` environment via
```sh
python3 -m venv venv
source venv/bin/activate
```

Note, you will need to call
```sh
source venv/bin/activate
```
each time you open the directory in a new shell.


Install all the required pip libraries via
```sh
pip install -r requirements.txt
```


## Opent the notebook


Initialize the venv environment (if not done already)
```sh
source venv/bin/activate
```

and start the jupyter from the project root directory

```sh
jupyter notebook
```
This should open your default browser pointing to the root directory containing all the notebooks.

