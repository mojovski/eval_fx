HOw to concatenate multiple csv zipfiles from histdata.com:

1. unzip

```
unzip "*.zip"

2. create a target csv in a subdirectory

mkdir joined/gbdpusd_1m_2020.csv

3. merge

```
find . -maxdepth 1 -name "*.csv"  -print0 | sort -z | xargs -r0  cat  >> joined/gbpusd_1m_2020.csv

```

