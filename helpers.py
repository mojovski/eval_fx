import numpy as np
import sys
import pandas as pd
import datetime
from datetime import timedelta
import hiplot as hip


lst=[]
mydateparser = lambda x: datetime.datetime.strptime(x, "%Y%m%d %H%M%S")+timedelta(hours=6) #convert to CET time

def summarizeCandles(candles, code="1H"):
    
    ohlcv_dict = {'open':'first', 'high':'max', 'low':'min', 'close': 'last', 'time':'first', 'vol':'sum'}

    candles.index = pd.to_datetime(candles["time"])
    #print(candles.head())
    #see https://stackoverflow.com/questions/36222928/pandas-ohlc-aggregation-on-ohlc-data/36223274
    df_sum=candles.resample(code, closed="right", convention="start").agg(ohlcv_dict)
    #df_sum.index=df_sum.index.shift(1) #to represent the candle END by the given time
    df_sum["time"]=df_sum["time"].shift(-1)
    df_sum.reset_index(drop=True, inplace=True)
    df_sum.dropna(inplace=True)
    df_sum.head()
    return df_sum

def load_file(file_csv, summary="1min"):
    """
    loads a file passed as an argument
    the csv file shoudl have following columns:
        time;open;high;low;close;vol
    """


    candles=pd.read_csv(file_csv, sep=';', parse_dates=['time'], date_parser=mydateparser)
    candles=summarizeCandles(candles, summary)
    candles["hl2"]=0.5*(candles["high"]+candles["low"])
    def domoncode(t):
        return str(t.year)+"-"+str(t.month)
    candles['month'] = candles.apply(lambda x: domoncode(x['time']), axis=1)
    return candles



def load(num_samples_to_consider, summary="15min", path_prefix="./"):
    """
    num_samples_to_consider: limit the number of candles to this number
    summary: the 1m candles from the CSV file are summarized to e.g. 15min candles
    path_prefix: the path to the root of the `data` directory.
    """
    
    print("Loading from "+path_prefix)
    #load full 2019
    sum_rows=0

    file_csv=path_prefix+"data/forex/2019/DAT_ASCII_EURUSD_M1_2019.csv" 
    print("Loading "+str(file_csv))
    candles_i=pd.read_csv(file_csv, sep=';', parse_dates=['time'], date_parser=mydateparser)
    sum_rows+=candles_i.shape[0]
    lst.append(candles_i)



    for i in []:#range(1,6):
        if sum_rows>num_samples_to_consider and num_samples_to_consider>0:
            break
        file_csv=path_prefix+"data/forex/2020/DAT_ASCII_EURUSD_M1_20200"+str(i)+".csv" 
        print("Loading "+str(file_csv))
        candles_i=pd.read_csv(file_csv, sep=';', parse_dates=['time'], date_parser=mydateparser)
        lst.append(candles_i)
        sum_rows+=candles_i.shape[0]

    #merge
    candles=pd.concat(lst).reset_index(drop=True)

    #summarize to a higher timeframe

    if summary!=None:
        candles=summarizeCandles(candles, summary)
    #print(candles.head())

    candles["hl2"]=0.5*(candles["high"]+candles["low"])

    #build month code
    global mns
    mns=0
    global prev_mon
    prev_mon=0
    def domoncode(t):
        global prev_mon
        global mns
        #encode time to an int, which raises consecutively
        if prev_mon!=t.month:
            prev_mon=t.month
            mns+=1
        return mns
    candles['0monthcode'] = candles.apply(lambda x: domoncode(x['time']), axis=1)
    candles['hour'] = candles.apply(lambda x: x['time'].hour, axis=1)



    print(str(candles.shape[0])+ " candles loaded!")
    if num_samples_to_consider==0:
        num_samples_to_consider=candles.shape[0]

    candles=candles[:num_samples_to_consider]
    return candles


def hiplot_samples(samples, target_name, hiplot_out):

    
    hp=hip.Experiment.from_iterable(samples)
    hp.parameters_definition[target_name].type = hip.ValueType.NUMERIC_PERCENTILE
    hp.parameters_definition[target_name].colormap = "interpolateTurbo"

    #write to html
    s=hp.to_html()
    file=open(hiplot_out, "w")
    file.write(s)
    file.close()
    print("Written the HiPlot to "+hiplot_out)

